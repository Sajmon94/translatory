﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace AnalizaLeksykalna
{
    class Program
    {
        static void Main(string[] args)
        {
            var symbols = LexicalAnalisys.GetSymbols();
            foreach (var item in symbols)
            {
                Console.WriteLine($"Symbol: {item}");
            }

            SyntaxAnalisys analiza = new SyntaxAnalisys(symbols);
            analiza.RunAnalisys();
            Console.ReadKey();
        }
    }

    public class SymbolWithLength
    {
        public int length { get; set; }
        public Symbols Value { get; set; }
    }

    public enum Symbols
    {
        Identyfikator,
        LiczbaCalkowita,
        LiczbaZmiennoprzecinkowa,
        Plus,
        Minus,
        Gwiazdka,
        Ukosnik,
        NawiasLewy,
        NawiasPrawy,
        Spacja,
        NowaLinia,
        ZnakBialy,
        Error
    }
}
