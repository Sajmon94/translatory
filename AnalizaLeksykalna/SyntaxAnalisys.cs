﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaLeksykalna
{
    class SyntaxAnalisys
    {
        static List<Symbols> symbols = new List<Symbols>();
        static int currentIndex;
        static Symbols current = Symbols.Spacja;
        bool success=true;

        public SyntaxAnalisys(List<Symbols> sym)
        {
            symbols = sym;
            currentIndex = 0;
            current = symbols[currentIndex];
        }

        public void RunAnalisys()
        {
            W();
            if(success)
            {
                Console.WriteLine("Dane Ok");
            }
        }

        void Load(Symbols symbol)
        {
            if(symbol == current)
            {
                current = GetNextSymbol();
            }
            else
            {
                LogError();
            }
        }

        void LogError()
        {
            Console.WriteLine("Błąd składni");
            success = false;
        }

        Symbols GetNextSymbol()
        {
            if (currentIndex < symbols.Count-1)
            {
                currentIndex++;
                return symbols[currentIndex];
            }
            else return Symbols.ZnakBialy;
        }

        void S()
        {
            if(current == Symbols.LiczbaCalkowita||current==Symbols.LiczbaZmiennoprzecinkowa||current==Symbols.Identyfikator||current==Symbols.NawiasLewy)
            {
                C();
                Y();
            }
            else
            {
                LogError();
            }
        }

        void X()
        {
            if(current == Symbols.Plus || current == Symbols.Minus)
            {
                current = GetNextSymbol();
                S();
                X();
            }
            else
            {
                //LogError();
            }
        }

        void Y()
        {
            if (current == Symbols.Gwiazdka || current == Symbols.Ukosnik)
            {
                current = GetNextSymbol();
                C();
                Y();
            }
            else
            {
                //LogError();
            }
        }

        void W()
        { 
          
            if(current == Symbols.LiczbaCalkowita|| current == Symbols.LiczbaZmiennoprzecinkowa ||current == Symbols.NawiasLewy)
            {
                S();
                X();
            }
            else
            {
                LogError();
            }
        }

        void C()
        {
            if(current == Symbols.NawiasLewy)
            {
                Load(Symbols.NawiasLewy);
                W();
                Load(Symbols.NawiasPrawy);
            }
            else if(current == Symbols.Identyfikator)
            {
                Load(Symbols.Identyfikator);
            }
            else if(current == Symbols.LiczbaCalkowita)
            {
                Load(Symbols.LiczbaCalkowita);
            }
            else 
            {
                Load(Symbols.LiczbaZmiennoprzecinkowa);
            }
        }
    }
}
