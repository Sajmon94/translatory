﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AnalizaLeksykalna
{
    class LexicalAnalisys
    {
        public static List<Symbols>GetSymbols()
        {
            string text;

            using (StreamReader sr = new StreamReader("Code.txt"))
            {
                String line = sr.ReadToEnd();
                Console.WriteLine(line);
                text = line;
            }

            text = text.ToLower();

            List<Symbols> results = new List<Symbols>();

            while (text.Length > 0)
            {
                var result = GetNextSymbol(text);
                if (result.Value != Symbols.ZnakBialy)
                {
                    results.Add(result.Value);
                }
                text = text.Substring(result.length, text.Length - result.length);
            }
            return results;
        }

        public static SymbolWithLength GetNextSymbol(string text)
        {
            Regex startsWithLetter = new Regex(@"^[a-z]");
            Regex startsWithDigit = new Regex(@"^\d");
            Regex isWhiteSpace = new Regex(@"^\s");

            Match isLetter = startsWithLetter.Match(text);
            Match isNumber = startsWithDigit.Match(text);
            Match isSpace = isWhiteSpace.Match(text);

            if (isLetter.Success)
            {
                Regex lettersWithNumbers = new Regex(@"^[a-z]+\d+");
                Regex onlyLetters = new Regex(@"^[a-z]+");

                Match longerOption = lettersWithNumbers.Match(text);
                Match shorterOption = onlyLetters.Match(text);
                if (longerOption.Success)
                {
                    return new SymbolWithLength()
                    {
                        length = longerOption.Length,
                        Value = Symbols.Identyfikator
                    };
                }
                else if (shorterOption.Success)
                {
                    return new SymbolWithLength()
                    {
                        length = shorterOption.Length,
                        Value = Symbols.Identyfikator
                    };
                }
                else
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Error
                    };
                }
            }
            else if (isNumber.Success)
            {
                Regex isInteger = new Regex(@"^\d+");
                Regex isDouble = new Regex(@"^\d+[.]\d+");

                Match Integer = isInteger.Match(text);
                Match Double = isDouble.Match(text);
                if (Double.Success)
                {
                    return new SymbolWithLength()
                    {
                        length = Double.Length,
                        Value = Symbols.LiczbaZmiennoprzecinkowa
                    };
                }
                else if (Integer.Success)
                {
                    return new SymbolWithLength()
                    {
                        length = Integer.Length,
                        Value = Symbols.LiczbaCalkowita
                    };
                }
                else
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Error
                    };
                }
            }
            else if (isSpace.Success)
            {
                return new SymbolWithLength()
                {
                    length = isSpace.Length,
                    Value = Symbols.ZnakBialy
                };
            }
            else
            {
                if (text[0].Equals('+'))
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Plus
                    };
                }
                else if (text[0].Equals('-'))
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Minus
                    };
                }
                else if (text[0].Equals('*'))
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Gwiazdka
                    };
                }
                else if (text[0].Equals('/'))
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Ukosnik
                    };
                }
                else if (text[0].Equals('('))
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.NawiasLewy
                    };
                }
                else if (text[0].Equals(')'))
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.NawiasPrawy
                    };
                }
                else
                {
                    return new SymbolWithLength()
                    {
                        length = 1,
                        Value = Symbols.Error
                    };
                }
            }

        }
    }
}
